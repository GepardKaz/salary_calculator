# README #

Тестовое задание - «Зарплатный калькулятор»

> Ссылка на тестовой сервер: [http://178.22.171.182:9090/](http://178.22.171.182:9090/)

> Login(email): `admin@mail.com`

> Пароль: `admin123`


## Коротко о проекте ##

* Язык программирования: PHP 8.1
* БД: MySQL 8
* Front-end: Bootstrap, JS(jQuery,Ajax)
* architectural pattern: MVC

## Как развернуть исходный код у себя? ##

### 1 способ
> 1. импорт дампа базы данных `db_dumps/salary_calculator.sql`
> 2. db config `application/config/config.php`
> 3. Run Server `php -S 127.0.0.1:8080 -t public`
> 4. [http://localhost:8080](http://localhost:8080)

### 2 способ
> 1. импорт дампа базы данных `db_dumps/salary_calculator.sql`
> 2. db config `application/config/config.php`
> 3. `docker-compose up --build`
> 4. [http://localhost:9080](http://localhost:9080)

## web page screen 
<img src="/uploads/1c7873b8bf2a62d0d83336f89538f0b8/Screen_Shot_2022-03-14_at_00.08.22.png" align="center" width="220" height="300">

> email(login): `admin@mail.com`

> Пароль: `admin123`

![Screen_Shot_2022-03-14_at_00.17.54](/uploads/c8d65f6684c1240fd2328c456b169875/Screen_Shot_2022-03-14_at_00.17.54.png)

![Screen_Shot_2022-03-14_at_00.22.43](/uploads/f163d68e72f4f283f7371a01eddb9b9d/Screen_Shot_2022-03-14_at_00.22.43.png)

## api test

### api validation
![Screen_Shot_2022-03-13_at_23.19.52](/uploads/10e0f20931b49f7ccd5c72173c5ab2ef/Screen_Shot_2022-03-13_at_23.19.52.png)

### api return after calc

![if_ip_2022-03-13_at_23.17.37](/uploads/4cf0e112119d409d869ac880db73dc9f/if_ip_2022-03-13_at_23.17.37.png)


