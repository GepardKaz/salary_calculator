$(document).ready(function() {

    var loc = window.location;
    var path = loc.protocol + '//' + loc.hostname + (loc.port ? ':' + loc.port : '') + loc.pathname;

    $('#btn-rcalc').click(function() {
        $('#rcalcModal').modal();
        $('#modal_result_blok').hide();
    });

    $('#show_result').click(function() {
        $.ajax({
            type: 'POST',
            url: path + "api?controller=api&action=calculate",
            data: $("#modalCalcForm").serialize(),
            beforeSend: function() {
                $("#show_result_spinner").show();
                $("#modal_error_alert").hide();
            },
        }).done(function(res) {
            console.log("Success");
            $("#modal_error_alert").hide();
            $("#modal_success_alert").show();
            $('#modal_success_alert').html("<p> Калькуляция успешна выполнена!</p>");
            $("#calc_btn_spinner").hide();
            $("#show_result_spinner").hide();
            $('#modal_result_blok').show();
            $('#modal_result_blok tbody').html("<tr><td>"
                                                    +res.accrued_salary.toFixed(2)+"</td><td>"
                                                    +res.ipn.toFixed(2)+"</td><td>"
                                                    +res.opv.toFixed(2)+"</td><td>"
                                                    +res.osms.toFixed(2)+"</td><td>"
                                                    +res.vosms.toFixed(2)+"</td><td>"
                                                    +res.so.toFixed(2)+"</td><td><b>"
                                                    +res.salary_on_hand.toFixed(2)+"</b></td></tr>");
        }).fail(function(msg) {
            $("#modal_success_alert").hide();
            $("#modal_error_alert").show();
            $('#modal_error_alert').html("<p> Возникла ошибка!</p>");
            console.log('fail');

            if(msg){
                for(i = 0; i < msg.responseJSON.length; i++) {
                    $('#modal_error_alert').append("<p>" + (i + 1) + ". " + msg.responseJSON[i] + "</p>");
                }
            }
        });
    });

    $('.disabled_person').click(function(){
        if($(this).attr("value")== 1){
            $('#disability_type_select').show();
        }
        if($(this).attr("value")== 0){
            $('#disability_type_select').hide();
        }  
    });

    $('.disabled_person_modal').click(function(){
        if($(this).attr("value")== 1){
            $('#disability_type_select_modal').show();
        }
        if($(this).attr("value")== 0){
            $('#disability_type_select_modal').hide();
        }  
    });
    
    $('.individual_entrepreneur_modal').click(function(){
        if($(this).attr("value")== 1){
            $('#radio_buttons_modal').hide();
        }
        if($(this).attr("value")== 0){
            $('#radio_buttons_modal').show();
        }  
    });

    $('.individual_entrepreneur').click(function(){
        if($(this).attr("value")== 1){
            $('#radio_buttons').hide();
        }
        if($(this).attr("value")== 0){
            $('#radio_buttons').show();
        }  
    });

    $('#calcWithSave').click(function() {     
        $.ajax({
            type: 'POST',
            url: path + "/api?controller=api&action=calculate",
            data: $("#calcForm").serialize(),
            beforeSend: function() {
                $("#calc_btn_spinner").show();
                $("#success_alert").hide();
                $("#error_alert").hide();
            },
        }).done(function(res) {
            console.log("Success");
            $("#error_alert").hide();
            $("#success_alert").show();
            $('#success_alert').html("<p> Калькуляция успешна выполнена!</p>");
            $("#calc_btn_spinner").hide();
            $('#result_table tbody').prepend("<tr><td>"+res[0].id+"</td><td>"
                                                    +res[0].accrued_salary+"</td><td>"
                                                    +res[0].ipn+"</td><td>"
                                                    +res[0].opv+"</td><td>"
                                                    +res[0].osms+"</td><td>"
                                                    +res[0].vosms+"</td><td>"
                                                    +res[0].so+"</td><td><b>"
                                                    +res[0].salary_on_hand+"</b></td></tr>");
        }).fail(function(msg) {
            $("#success_alert").hide();
            $("#error_alert").show();
            $('#error_alert').html("<p> Возникла ошибка!</p>");
            console.log('fail');

            if(msg){
                for(i = 0; i < msg.responseJSON.length; i++) {
                    $('#error_alert').append("<p>" + (i + 1) + ". " + msg.responseJSON[i] + "</p>");
                }
            }  
        });
    });

    $('.addVacationPeriod').click(function() {
        $('#multiple_vacation').append('<input type="date" name="vacation_begin"> ~ <input type="date" name="vacation_end"><span class="badge badge-success addVacationPeriod" style="font-size: 16px;"><i class="fa fa-plus"></i></span><br>');
    });
});