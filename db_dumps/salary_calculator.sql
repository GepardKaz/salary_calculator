-- Adminer 4.8.1 MySQL 8.0.26 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `calculated_salaries`;
CREATE TABLE `calculated_salaries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `oklad` decimal(12,2) NOT NULL,
  `worked_year` int NOT NULL,
  `worked_month` int NOT NULL,
  `work_days` int NOT NULL DEFAULT '22',
  `worked_days` int NOT NULL,
  `is_individual_entrepreneur` tinyint(1) NOT NULL DEFAULT '0',
  `is_mzp` tinyint(1) NOT NULL DEFAULT '0',
  `retireed` tinyint(1) NOT NULL DEFAULT '0',
  `disabled_person` tinyint(1) NOT NULL DEFAULT '0',
  `disability_type` int DEFAULT '0',
  `ipn` decimal(12,2) NOT NULL DEFAULT '0.00',
  `opv` decimal(12,2) NOT NULL DEFAULT '0.00',
  `vosms` decimal(12,2) NOT NULL DEFAULT '0.00',
  `osms` decimal(12,2) NOT NULL DEFAULT '0.00',
  `so` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accrued_salary` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salary_on_hand` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `password`, `email`) VALUES
(1,	'240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9',	'admin@mail.com');

DROP TABLE IF EXISTS `vacation_time_list`;
CREATE TABLE `vacation_time_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `calculated_salary_id` int NOT NULL,
  `begin_date1` date NOT NULL,
  `end_date1` date NOT NULL,
  `begin_date2` date NOT NULL,
  `end_date2` date NOT NULL,
  `vacation_type_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `vacation_types`;
CREATE TABLE `vacation_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mzp` tinyint(1) NOT NULL DEFAULT '0',
  `ipn` tinyint(1) NOT NULL DEFAULT '0',
  `opv` tinyint(1) NOT NULL DEFAULT '0',
  `vosms` tinyint(1) NOT NULL DEFAULT '0',
  `osms` tinyint(1) NOT NULL DEFAULT '0',
  `so` tinyint(1) NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2022-03-13 16:58:57
