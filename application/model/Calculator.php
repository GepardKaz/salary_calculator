<?php

class Calculator
{
    function __construct($db)
    {
        try
        {
            $this->db = $db;
        } catch (PDOException $e)
        {
            exit('Database connection could not be established.');
        }
    }

    public function getAll()
    {
        $sql = "select * from calculated_salaries order by id desc";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function saveCalculate($oklad,$year,$month,$work_days,
                                    $worked_days,$is_mzp,$disabled_person,
                                    $disability_type,$retireed,$opv,$osms,
                                    $vosms,$so,$ipn,$mrp,$mzp,$salary_on_hand,
                                    $accrued_salary,$vacation_time,$is_individual_entrepreneur)
    {
        $sql = "INSERT INTO calculated_salaries 
                        SET oklad = :oklad, 
                            worked_year = :worked_year,
                            worked_month = :worked_month,
                            work_days = :work_days,
                            worked_days = :worked_days,
                            is_individual_entrepreneur = :is_individual_entrepreneur,
                            is_mzp = :is_mzp,
                            retireed = :retireed,
                            disabled_person = :disabled_person,
                            disability_type = :disability_type,
                            ipn = :ipn, opv = :opv, vosms = :vosms,
                            osms = :osms,so = :so, accrued_salary = :accrued_salary, salary_on_hand = :salary_on_hand";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':oklad', round($oklad, 2)); 
        $stmt->bindValue(':worked_year', $year); 
        $stmt->bindValue(':worked_month', $month); 
        $stmt->bindValue(':work_days', $work_days);                                            
        $stmt->bindValue(':worked_days', $worked_days);
        $stmt->bindValue(':is_individual_entrepreneur', $is_individual_entrepreneur); 
        $stmt->bindValue(':is_mzp', $is_mzp); 
        $stmt->bindValue(':retireed', $is_mzp); 
        $stmt->bindValue(':disabled_person', $disabled_person); 
        $stmt->bindValue(':disability_type', $disability_type);
        $stmt->bindValue(':ipn', round($ipn, 2));
        $stmt->bindValue(':opv', round($opv, 2)); 
        $stmt->bindValue(':vosms', round($vosms, 2));
        $stmt->bindValue(':osms', round($osms, 2)); 
        $stmt->bindValue(':so', round($so, 2)); 
        $stmt->bindValue(':accrued_salary', round($accrued_salary, 2)); 
        $stmt->bindValue(':salary_on_hand', round($salary_on_hand, 2)); 
        //$stmt->bindValue(':vacation_time', $vacation_time);

        $stmt->execute();
        $id = $this->db->lastInsertId();

        if($id){
            $get_last = "select * from calculated_salaries where id = $id limit 1";
            $query = $this->db->prepare($get_last);
            $query->execute();
            return $query->fetchAll();
        }
    }
}