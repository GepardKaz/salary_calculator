<?php

require APP . 'model/User.php';
require APP . 'model/Calculator.php';


/**
 * Class Home
 */
class Home extends Controller
{

    public function index()
    {
        $calc = new Calculator($this->db);
        $calculated_salaries = $calc ->getAll();
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/footer.php';
    }

}
