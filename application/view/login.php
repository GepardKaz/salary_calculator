
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Salary calculator</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/sign-in/signin.css" rel="stylesheet">
</head>
<body class="text-center">
    <div class="card">
        <div class="card-header bg-primary text-light">
            <h4>Авторизация</h4>
        </div>
        <div class="card-body">
            <div class="form-group">
                <form class="form-signin" method="post" action="<?php echo URL; ?>?controller=login">
                    <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">
                    <?php
                    if (isset($erros)) {
                        ?>
                        <div class="alert alert-danger">
                            <?php echo $erros; ?>
                        </div>
                        <?php
                    } unset($erros);
                    ?>
                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
                    <input type="password" name="pwd" id="inputPassword" class="form-control mt-3" placeholder="Пароль" required>
                    <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Войти</button> 
                </form>
            </div>
            <p class="mt-2 text-muted">&copy; Salary Calculator, 2022</p>
        </div>
    </div>
</body>
</html>
